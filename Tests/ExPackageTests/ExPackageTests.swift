import XCTest
@testable import ExPackage

final class ExPackageTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(ExPackage().text, "Hello, World!")
    }
}
