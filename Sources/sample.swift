//
//  File.swift
//  
//
//  Created by Vinayakam on 04/01/22.
//

import Foundation
import SwiftUI

@available(iOS 13.0, *)
public struct SwiftUIView: View {
    public init() {}
    public var body: some View {
        Text("Sample body")
    }
}
